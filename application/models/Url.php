<?php

class Url {
  private $pathUrl;
  private $dateUrl;
  private $shortenUrl;
  private $author;

  public function __construct(string $pathUrl="", string $dateUrl="", string $shortenUrl="", string $author=""){
    $this->pathUrl = $pathUrl;
    $this->dateUrl = $dateUrl;
    $this->shortenUrl = $shortenUrl;
    $this->author = $author;
  }

    public function getpathUrl(){
      return $this->pathUrl;
    }

    public function getdateUrl(){
      return $this->dateUrl;
    }

    public function getshortenUrl(){
      return $this->shortenUrl;
    }



}
