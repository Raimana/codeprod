<?php $this->load->helper('form'); ?>

<div class="jumbotron">
  <div class="container">
    <h1>Raccourcisseur url Perso</h1>
  </div>
</div>
<div class="container">

<?php
echo form_open('traitementFormulaire');
echo form_label('Votre URL :', 'url');
echo form_input('url', 'saisir URL');
echo form_submit('btn', 'Raccourcir mon URL');
echo form_close();
 ?>
</div>
<div class="container">
  <table class="table">
    <thead>
      <tr>
        <th>Date</th>
        <th>Url long</th>
        <th>Url court</th>
      </tr>
    </thead>
    <tbody>
      <?php
        foreach ($urls as $url) {
          echo "<tr><td>".$url->getdateUrl()."</td>";
          echo "<td>".$url->getPathUrl()."</td>";
          echo "<td><a href='site/redirect/".$url->getshortenUrl()."'>Lien</a></td></tr>";
        }
       ?>
    </tbody>
  </table>

</div>
