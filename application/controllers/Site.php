<?php

class Site extends CI_Controller {

    public function index() {
    $data["title"] = "Mon raccourcisseur url";

    $this->load->view('common/header', $data);
    $this->load->view('site/index', $data);
    $this->load->view('common/footer', $data);
  }

  public function google() {
    $data["title"] = "Raccourcisseur url Google";

    $this->load->view('common/header', $data);
    $this->load->view('site/google', $data);
    $this->load->view('common/footer', $data);
  }

  public function formulairePerso() {
    //librairie elasticSearch
$this->load->library('ElasticSearch');
//chargement model Url
$this->load->model('Url');
//récupérer toutes les urls
$results = $this->elasticsearch->query_all("*:*");
//ne prendre que les enregistrements qui nous intéressent
$results = $results["hits"]["hits"];
//création tableau d'urls
$lesURL = array();
for ($i=0; $i < count($results); $i++) {
//création des instances d'url
$lesURL[] = new Url($results[$i]["_source"]["url"],$results[$i]["_source"]["dateURL"],$results[$i]["_source"]["URL de raccourcie"]);
}
//ajouts des url aux data à envoyer à la vue
$data["urls"] = $lesURL;
$data["title"] = "Raccourcisseur url Perso";

$this->load->view('common/header', $data);
$this->load->view('site/formulaire', $data);
$this->load->view('common/footer', $data);


  }

  public function traitementFormulaire() {
//Chargement ElasticSearch
$this->load->library('ElasticSearch');

$this->load->library('Tools');
//préparation des données à insérer dans ElasticSearch
$data = array("url"=>$this->input->post('url'), "dateURL"=>date('Y-m-d'), "URL de raccourcie"=>$this->tools->generateCode());
//récupération du nombre d'enregistrements
$id = $this->elasticsearch->count("url")["count"] ;
//incrémentation
$id +=1;
//ajout du nouvel enregistrement
$this->elasticsearch->add("url", $id, $data);
//attendre 1 seconde avant de recharger l'interface
sleep(1);
//chargement interface
$this->formulairePerso();
}


  public function redirect($code) {
    $this->load->library('ElasticSearch');

    //recherche de l'url dont le code est passé en paramètre
    $url = $this->elasticsearch->advancedquery("url", '{
  "query": {
    "query_string": {
      "query": "'.$code.'",
      "fields": [
        "URL de raccourcie"
      ]
    }
  }
}');

    //récupération uniquement de l'url
    $data["url"] = $url["hits"]["hits"][0]["_source"]["url"];
    //appel de la vue en lui passant l'url
    $this->load->view('site/redirect', $data);
  }

  public function isURL($url){
    if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
      $this->form_validation->set_message('isURL', 'The URL provided is not valid');
        return FALSE;
      }else{
        return TRUE;
    }
  }



}
